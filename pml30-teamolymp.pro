#-------------------------------------------------
#
# Project created by QtCreator 2016-02-21T15:49:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pml30-teamolymp
TEMPLATE = app


SOURCES += src/main.cpp\
    src/grid.cpp \
    src/olymp.cpp \
    src/synch.cpp \
    src/taskinsp.cpp \
    src/tasksingle.cpp \
    src/windowmain.cpp

HEADERS  += \
    src/grid.h \
    src/olymp.h \
    src/windowmain.h

FORMS    +=
QMAKE_CXXFLAGS += -std=c++11 -Wno-unused-parameter
