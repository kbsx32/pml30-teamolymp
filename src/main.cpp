/* pml30 teamolymp project
 * main entry point program.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#include <QApplication>

#include "windowmain.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    pto::WindowMain w;
    w.show();

    return a.exec();
}
