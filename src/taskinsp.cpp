/* pml30 teamolymp project.
 *
 * 'TaskInsp' widget implementation.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#include <QHeaderView>

#include "grid.h"

int pto::glms::Resulter::fontPtSize = 20;
int pto::glms::TaskSingle::fontPtSize = 20;
int pto::glms::TeamIntro::fontPtSize = 20;
int pto::glms::TasksInsp::fontPtSize = 20;

pto::glms::Resulter::Resulter(Grid *grid, uint32_t teamId, QWidget *parent) :
	QLineEdit(parent),
	grid(grid),
	teamId(teamId)
{
	connect(grid, SIGNAL(signalUpdateInfo()),
			this, SLOT(updateInfo()));

    setEnabled(false);
    QFont fnt = font();
    fnt.setBold(true);
    fnt.setUnderline(true);
    setFont(fnt);
}

void pto::glms::Resulter::updateInfo()
{
	float resSum = 0;

	for (uint32_t i = 0; i < grid->tasksCnt; ++i)
		resSum += grid->getDecResult(teamId, i);

	setText(QString::number(resSum));
    QFont fnt = font();
    fnt.setPointSize(fontPtSize);

    setFont(fnt);
}

pto::glms::TasksInsp::TasksInsp(Grid *grid, uint32_t teamId, QWidget *parent) :
	QTableWidget(parent)
{
	setRowCount(1);
	setColumnCount(grid->tasksCnt);

	verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

	horizontalHeader()->hide();
	verticalHeader()->hide();

	// setSizePolicy(QSizePolicy);
	for (uint32_t i = 0; i < grid->tasksCnt; ++i) {
		setCellWidget(0, i, new TaskSingle(grid, teamId, i, this));
	}
}

pto::glms::TeamIntro::TeamIntro(Grid *grid,
								uint32_t teamId,
								QWidget *parent) :
	QLineEdit(parent),
	grid(grid),
	teamId(teamId)
{
    slotChangeName(false);

	connect(this, SIGNAL(editingFinished()),
			this, SLOT(slotChangeName()));

	connect(grid, SIGNAL(signalUpdateInfo()),
			this, SLOT(slotUpdateInfo()));
}

void pto::glms::TeamIntro::slotChangeName(bool update)
{
    grid->getTeam(teamId).name = text();

    if (update)
        grid->slotUpdate();
}

void pto::glms::TeamIntro::slotUpdateInfo()
{
	setText(grid->teams[teamId].name);

    QFont fnt = font();
    fnt.setPointSize(fontPtSize);
    setFont(fnt);
}
