/* pml30 teamolymp project.
 *
 * main window widget implementation.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#include <QScrollArea>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QPushButton>
#include <QSpinBox>

#include "windowmain.h"

pto::WindowMain::WindowMain(QWidget *parent) :
	QWidget(parent),
	grid(new glms::Grid(this))
{
	QVBoxLayout *lay = new QVBoxLayout();
	setLayout(lay);

	QHBoxLayout *butLay = new QHBoxLayout();
	lay->addLayout(butLay);
	lay->addWidget(grid);

	QPushButton
			*load = new QPushButton("load", this),
            *save = new QPushButton("save", this),
            *saveAs = new QPushButton("save as", this),
            *clearResults = new QPushButton("reset", this);

	connect(load, SIGNAL(clicked()), this, SLOT(slotLoad()));
	connect(save, SIGNAL(clicked()), this, SLOT(slotSave()));
    connect(saveAs, SIGNAL(clicked()), this, SLOT(slotSaveAs()));
    connect(clearResults, SIGNAL(clicked()), this, SLOT(slotResetResults()));

	butLay->addWidget(load);
	butLay->addWidget(save);
    butLay->addWidget(saveAs);
    butLay->addWidget(clearResults);

	QSpinBox *spinCount = new QSpinBox(this);
	spinCount->setPrefix("visible teams : ");
	spinCount->setRange(1, grid->teamsCnt);
	connect(spinCount, SIGNAL(valueChanged(int)),
			this, SLOT(slotSpinCount(int)));
	spinCount->setValue(grid->teamsCnt);
    butLay->addWidget(spinCount);

    butLay->addWidget(makeSpinFontSizer("team name sz : ", SLOT(slotUpdateFontSizeTeamIntro(int))));
    butLay->addWidget(makeSpinFontSizer("task sz : ", SLOT(slotUpdateFontSizeTaskSingle(int))));
    butLay->addWidget(makeSpinFontSizer("resulter sz : ", SLOT(slotUpdateFontSizeResulter(int))));
}

pto::WindowMain::~WindowMain()
{
}

void pto::WindowMain::slotSave()
{
	if (saveFileName == "")
		saveFileName = QFileDialog::getSaveFileName(this, "save file (*.csv)");

	if (saveFileName == "")
		return ;

    /* append csv */
    if (saveFileName.contains(".csv") == false)
        saveFileName += ".csv";

	grid->save(saveFileName);
}

void pto::WindowMain::slotSaveAs()
{
    QString newFileName =
        QFileDialog::getSaveFileName(this, "save file (*.csv)");

    if (newFileName == "")
        return ;

    saveFileName = newFileName;

    slotSave();
}

void pto::WindowMain::slotLoad()
{
	saveFileName = QFileDialog::getOpenFileName(this, "load file (*.csv)");

	if (saveFileName == "")
		return ;

	grid->load(saveFileName);
	grid->slotUpdate();
}

void pto::WindowMain::slotResetResults()
{
    grid->resetResults();
    grid->slotUpdate();
}

void pto::WindowMain::slotSpinCount(int cntNew)
{
	grid->changeVisibleTeams(cntNew);
}

QWidget * pto::WindowMain::makeSpinFontSizer(const QString &prefix, const char *slot)
{
    QSpinBox *spinCount = new QSpinBox(this);
    spinCount->setPrefix(prefix);
    spinCount->setRange(0, 32);
    connect(spinCount, SIGNAL(valueChanged(int)),
            this, slot);
    spinCount->setValue(26);

    return spinCount;
}

void pto::WindowMain::slotUpdateFontSizeResulter(int valNew)
{
    glms::Resulter::fontPtSize = valNew;
    grid->slotUpdate();
}

// void pto::glms::Grid::makeScroller()
void pto::WindowMain::slotUpdateFontSizeTaskSingle(int valNew)
{
    glms::TaskSingle::fontPtSize = valNew;
    grid->slotUpdate();
}

void pto::WindowMain::slotUpdateFontSizeTeamIntro(int valNew)
{
    glms::TeamIntro::fontPtSize = valNew;
    grid->slotUpdate();
}

void pto::WindowMain::slotUpdateFontSizeTasksInsp(int valNew)
{
    glms::TasksInsp::fontPtSize = valNew;
    grid->slotUpdate();
}
