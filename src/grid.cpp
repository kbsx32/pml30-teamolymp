/* pml30 teamolymp project.
 *
 * simple widgets implement.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#include <QHeaderView>

#include "grid.h"

pto::glms::Grid::Grid(QWidget *parent) :
	QTableWidget(parent)
{
    setColumnCount(EC(Columns::COUNT));
	setRowCount(teamsCnt);

	verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    /* set possibility to stretch name-sector */
	horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	horizontalHeader()->setSectionResizeMode(0, QHeaderView::Interactive);
	horizontalHeader()->setSectionResizeMode(2, QHeaderView::Interactive);

    /* init avery string for every team */
	for (uint32_t i = 0; i < teamsCnt; ++i)
	{
		setCellWidget(i, EC(Columns::TEAM_NAME), new TeamIntro(this, i));
		setCellWidget(i, EC(Columns::TASKS),	 new TasksInsp(this, i));
		setCellWidget(i, EC(Columns::RESULT),	 new Resulter(this, i));
	}
}

/* updating info in all table */
void pto::glms::Grid::update()
{
	Olympiad::sort();
}

/* updating info in all table */
void pto::glms::Grid::slotUpdate()
{
	update();
	emit signalUpdateInfo();
}

void pto::glms::Grid::changeVisibleTeams(uint32_t cntNew)
{
	for (uint32_t i = 0; i < cntNew; ++i)
		showRow(i);
	for (uint32_t i = cntNew; i < teamsCnt; ++i)
		hideRow(i);
}

