/* pml30 teamolymp project.
 *
 * all 'core' functions for synchronization
 * by '.csv' format.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#include <cstdio>
#include <fstream>

#include "grid.h"

bool pto::Olympiad::save(const QString &fileName)
{
	FILE *fout;

	fout = fopen(fileName.toLatin1().data(), "w");
	if (fout == nullptr)
		// throw std::exception("can't create file");
		return false;

	for (uint32_t i = 0; i < teamsCnt; ++i)
	{
		fprintf(fout, "%s,", teams[i].name.toLocal8Bit().data());

		for (uint32_t j = 0; j < tasksCnt; ++j)
		{
			fprintf(fout, "%.2f", getDecResult(i, j));

			char sign[3];
			sign[0] = 0;
			switch (getResult(i, j)) {
			case TaskState::CLEAR: break;
			case TaskState::FIRST_TRY: strcat(sign, "-"); break;
			case TaskState::BROKEN: strcat(sign, "--"); break;
			case TaskState::PASSED: strcat(sign, "+"); break;
			}

			fprintf(fout, "%s,", sign);
		}
		fprintf(fout, "%.2f", getSumResult(i));
		fprintf(fout, "\n");

	}

	fclose(fout);

	return true;
}

bool pto::Olympiad::load(const QString &fileName)
{
	FILE *fin;

	fin = fopen(fileName.toLatin1().data(), "r");
	if (fin == nullptr)
		return false;
		// throw std::exception("can't open file");

	for (uint32_t i = 0; i < teamsCnt; ++i)
	{
		char str[128];
		readUntilComma(fin, str, 128);

		teams[i].name = str;

		for (uint32_t j = 0; j < tasksCnt; ++j) {
			float val;
			char tryVar[8];
			fscanf(fin, "%f", &val);
			readUntilComma(fin, tryVar, 8);

			if (!strcmp(tryVar, ""))
				teams[i].tasks[j] = TaskState::CLEAR;
			else if (!strcmp(tryVar, "+"))
				teams[i].tasks[j] = TaskState::PASSED;
			else if (!strcmp(tryVar, "-"))
				teams[i].tasks[j] = TaskState::FIRST_TRY;
			else if (!strcmp(tryVar, "--"))
				teams[i].tasks[j] = TaskState::BROKEN;
		}

		float sumRes;
		fscanf(fin, "%f\n", &sumRes);
		// ++sumRes;
	}

	fclose(fin);

	return true;
}

char * pto::Olympiad::readUntilComma(FILE *fin, char *str, uint32_t maxLen)
{
	uint32_t len = 0;

	char ch;
	while (len < maxLen - 1) {
		ch = getc(fin);
		// if (ch == -1 || ch == ' ' || ch == ',' || ch == '\n')
		if (ch == -1 || ch == ',' || ch == '\n')
			break;

		str[len++] = ch;
	}

	str[len] = 0;

	return str;
}

