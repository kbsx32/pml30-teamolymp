/* pml30 teamolymp project.
 *
 * all 'core' functions for olympiad.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#pragma once
#ifndef PTO_OLYMP_H
#define PTO_OLYMP_H

#include <cstdio>
#include <stdint.h>

#include <QString>

#define EC(ENUM_VAL)	\
	(static_cast<uint32_t>(ENUM_VAL))

namespace pto
{
	enum class TaskState
	{
		CLEAR,		/* no tries for this task */
		FIRST_TRY,  /* was first try - not passed */
		BROKEN,		/* loosed two tries */
		PASSED,		/* task is done */
	};

	class Olympiad
	{
	public:
		static const uint32_t tasksCnt = 8;
		static const uint32_t teamsCnt = 17;

		class Team
		{
		public:
			QString name;
			TaskState tasks[tasksCnt];

			Team();

			float valResult = 0;

			bool operator<(const Team &t1) const
			{
				if (valResult != t1.valResult)
					return valResult > t1.valResult;
				return name > t1.name;
			}

            void resetResults();
		};

	public:
		Olympiad();

		/* all teams */
		Team teams[teamsCnt];

		TaskState getResult(const uint32_t teamId, const uint32_t task);


		/* sum result function for team */
		float getSumResult(const uint32_t teamId);

		void setResult(const uint32_t teamId,
					   const uint32_t task,
					   const TaskState ts);

		/* get decimal result view */
		float getDecResult(const uint32_t teamId,
						   const uint32_t task);

		/* get team */
		Team & getTeam(const uint32_t teamId);

		void sort();

        void resetResults();

        /* csv format synchro */
        bool save(const QString &fileName);
        bool load(const QString &fileName);
    private:
        /* for *.csv file reading additional function */
		char *readUntilComma(FILE *fin, char *str, uint32_t maxLen);
	}; /* end of 'Olympiad' class */
}

#endif /*  PTO_OLYMP_H */

