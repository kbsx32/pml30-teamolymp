/* pml30 teamolymp project
 * main window file.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#ifndef TO_WINDOWMAIN_H
#define TO_WINDOWMAIN_H

#include <functional>
#include <QWidget>

#include "grid.h"

namespace pto
{
    /* main linkage class */
	class WindowMain : public QWidget
	{
		Q_OBJECT

		glms::Grid *grid;

		QString saveFileName;
	public:
		WindowMain(QWidget *parent = 0);
		~WindowMain();

        QWidget * makeSpinFontSizer(const QString &prefix, const char *slot);
	public slots:
		void slotSave();
        void slotSaveAs();

        void slotLoad();
        void slotResetResults();

        void slotSpinCount(int cntNew);

        void slotUpdateFontSizeResulter(int valNew);
        void slotUpdateFontSizeTaskSingle(int valNew);
        void slotUpdateFontSizeTeamIntro(int valNew);
        void slotUpdateFontSizeTasksInsp(int valNew);
    };
} /* end of 'pto' */

#endif /* WINDOWMAIN_H */
