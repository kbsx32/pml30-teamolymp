/* pml30 teamolymp project.
 *
 * 'TaskSingle' widget implementation.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#include <QHeaderView>

#include "grid.h"

pto::glms::TaskSingle::TaskSingle(Grid *grid,
								  uint32_t teamId,
								  uint32_t task,
								  QWidget *parent) :
	QPushButton(parent),
	grid(grid),
	teamId(teamId),
	taskId(task)
{
	connect(grid, SIGNAL(signalUpdateInfo()),
			this, SLOT(slotUpdateInfo()));

	menu = new QMenu(this);

	QAction *act;

	act = new QAction("  : clear",		this);
	connect(act, SIGNAL(triggered(bool)), this, SLOT(slotMenuClear()));
	menu->addAction(act);

	act = new QAction("0 : first-try", this);
	connect(act, SIGNAL(triggered(bool)), this, SLOT(slotMenuFirstTry()));
	menu->addAction(act);

	act = new QAction("X : broken",	this);
	connect(act, SIGNAL(triggered(bool)), this, SLOT(slotMenuBroken()));
	menu->addAction(act);

	act = new QAction("1 : passed",	this);
	connect(act, SIGNAL(triggered(bool)), this, SLOT(slotMenuPassed()));
	menu->addAction(act);

	connect(this, SIGNAL(clicked(bool)),
			this, SLOT(slotMenuCalled()));

	setText(QString::number(task));

	setAutoFillBackground(true);

	/* coloriizing =) */
	setResult(grid->getResult(teamId, taskId), false);
}

void pto::glms::TaskSingle::slotMenuCalled()
{
	menu->exec(QCursor::pos());
} /* end of 'slotMenuCalled' */

void pto::glms::TaskSingle::slotMenuFinished(QAction *act)
{
} /* end of 'slotMenuCalled' */

void pto::glms::TaskSingle::slotMenuClear()
{
	setResult(TaskState::CLEAR);
}

void pto::glms::TaskSingle::slotMenuFirstTry()
{
	setResult(TaskState::FIRST_TRY);
}

void pto::glms::TaskSingle::slotMenuBroken()
{
	setResult(TaskState::BROKEN);
}

void pto::glms::TaskSingle::slotMenuPassed()
{
	setResult(TaskState::PASSED);
}

void pto::glms::TaskSingle::setResult(const TaskState ts, bool updateGlobalTable)
{
	setFlat(false);

	grid->setResult(teamId, taskId, ts);

	slotUpdateInfo();

	if (updateGlobalTable)
		grid->slotUpdate();
}

void pto::glms::TaskSingle::slotUpdateInfo()
{
	QString bkProperty("background: ");

	switch (grid->getResult(teamId, taskId))
	{
	case TaskState::CLEAR: /* white */
			setStyleSheet(bkProperty + "#FFFFFF");
			break;
	case TaskState::FIRST_TRY: /* yellow */
			setStyleSheet(bkProperty + "#C0C000");
			break;
	case TaskState::BROKEN: /* red */
			setStyleSheet(bkProperty + "#C00000");
			QColor(0xA00000);
			break;
	case TaskState::PASSED: /* green */
			setStyleSheet(bkProperty + "#00C000");
			break;
	}

	setText(QString::number(grid->getDecResult(teamId, taskId)));

    QFont fnt = font();
    fnt.setPointSize(fontPtSize);
    setFont(fnt);

    if (grid->teams[teamId].name == "")
        setEnabled(false);
    else
        setEnabled(true);
}
