/* pml30 teamolymp project.
 *
 * simple widgets definitions.
 *      TeamIntro   :   team name widget.
 *      TaskSingle  :   cell for task for single team.
 *      TaskInsp    :   resulter cell.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#pragma once
#ifndef PTO_GRID_H
#define PTO_GRID_H

#include <QTableWidget>
#include <QTableWidgetItem>
#include <QTextLine>
#include <QLineEdit>
#include <QMenu>
#include <QLabel>
#include <QPushButton>

#include "olymp.h"

namespace pto
{
	/* grid elements namespace */
	namespace glms
	{
		class Grid;

		/* class 'inspector' */
		class TeamIntro : public QLineEdit
		{
			Q_OBJECT

			Grid *grid;
			uint32_t teamId;
		public:
            static int fontPtSize;

            TeamIntro(Grid *grid,
					  uint32_t teamId,
					  QWidget *parent = nullptr);

		public slots:
            void slotChangeName(bool update = true);
			void slotUpdateInfo();
		};

		/* class 'inspector' */
		class TaskSingle : public QPushButton
		{
			Q_OBJECT

			QMenu *menu;
			Grid *grid;
			uint32_t teamId, taskId;

			void setResult(const TaskState ts, bool updateGlobalTable = true);

		public:
            static int fontPtSize;

            TaskSingle(Grid *grid,
					   uint32_t teamId,
					   uint32_t task,
					   QWidget *parent = nullptr);

		public slots:
			void slotMenuCalled();
			void slotMenuFinished(QAction *act);

			void slotMenuClear();
			void slotMenuFirstTry();
			void slotMenuBroken();
			void slotMenuPassed();

			void slotUpdateInfo();
		}; /* end of 'TaskInsp' class */

		/* summary all tasks widget */
		class TasksInsp : public QTableWidget
		{
			Q_OBJECT
		private:
		public:
            static int fontPtSize;

			TasksInsp(Grid *grid, uint32_t teamId, QWidget *parent = nullptr);
		};

		/* class 'inspector' */
		class Resulter : public QLineEdit
		{
			Q_OBJECT

			Grid *grid;
			uint32_t teamId;
		public:
            static int fontPtSize;

			Resulter(Grid *grid, uint32_t teamId, QWidget *parent = nullptr);

		public slots:
			void updateInfo();
		}; /* end of 'TaskInsp' class */

		/* Result grid.
		 * tasks count is constant.
		 */
		class Grid : public QTableWidget, public Olympiad
		{
			Q_OBJECT
		private:

			// std::vector<teams>
			enum class Columns
			{
				TEAM_NAME,
				TASKS,
				RESULT,
                END,
                COUNT = END,
			}; /* end of 'Columns' */

		public:
			Grid(QWidget *parent = nullptr);

			void update();

			void changeVisibleTeams(uint32_t cntNew);
		signals:
			void signalUpdateInfo();
		public slots:
			void slotUpdate();
        }; /* end of 'Grid' */
	} /* end of 'glms' */
}
#endif /* PTO_GRID_H */
