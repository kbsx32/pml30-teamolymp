/* pml30 teamolymp project.
 *
 * all 'core' functions for olympiad.
 *
 * kbsx32 <kbsx32@yandex.ru>
 */

#include <cstring>

#include "olymp.h"

pto::Olympiad::Team::Team()
{
    resetResults();
}

void pto::Olympiad::Team::resetResults()
{
    memset(tasks, 0, sizeof(tasks[0]) * tasksCnt);
}

pto::Olympiad::Olympiad()
{
	// std::memset(teams, 0, sizeof(teams[0]) * teamsCnt);
}

void pto::Olympiad::setResult(const uint32_t teamId,
							  const uint32_t task,
							  const TaskState ts)
{
	teams[teamId].tasks[task] = ts;
}

pto::TaskState pto::Olympiad::getResult(const uint32_t teamId,
							  const uint32_t task)
{
	return teams[teamId].tasks[task];
}


/* get decimal result view */
float pto::Olympiad::getDecResult(const uint32_t teamId,
								  const uint32_t task)
{
	float a = teamsCnt,
		  k = 2 / (3 * (1 - a)),
		  b = 1 - k;

	/* count count of good solutions for task */
	int cntPassed = 0;
	for (uint32_t i = 0; i < teamsCnt; ++i)
		if (teams[i].tasks[task] == TaskState::PASSED)
			++cntPassed;

	float resTeam = teams[teamId].tasks[task] == TaskState::PASSED ? 1 : 0;
	return 30 * resTeam * (k * cntPassed + b);
}

/* get team */
pto::Olympiad::Team & pto::Olympiad::getTeam(const uint32_t teamId)
{
	return teams[teamId];
}

/* sum result function for team */
float pto::Olympiad::getSumResult(const uint32_t teamId)
{
	float sum = 0;
	for (uint32_t i = 0; i < tasksCnt; ++i)
		sum += getDecResult(teamId, i);

	return sum;
}

void pto::Olympiad::sort()
{
	for (uint32_t i = 0; i < teamsCnt; ++i)
		teams[i].valResult = getSumResult(i);

	std::sort(std::begin(teams), std::end(teams));
}

void pto::Olympiad::resetResults()
{
    for (auto &team : teams)
        team.resetResults();
}
